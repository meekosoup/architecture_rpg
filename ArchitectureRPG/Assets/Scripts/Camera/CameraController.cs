﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float _maxTilt = 15f;
    [SerializeField] private float _minTilt = -15f;

    private float _tilt;
    private void Update()
    {
        float mouseRotation = Input.GetAxis("Mouse Y");
        _tilt = Mathf.Clamp(_tilt - mouseRotation, _minTilt, _maxTilt);
        
        transform.localRotation = Quaternion.Euler(_tilt, 0f, 0f);
    }
}
