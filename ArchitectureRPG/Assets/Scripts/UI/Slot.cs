﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Serialization;

public class Slot : MonoBehaviour
{
    [SerializeField, FormerlySerializedAs("_icon")] private Image _iconImage = null;
    [SerializeField] private TMP_Text _text;
    [SerializeField] private bool _validated;

    public Item Item;
    public bool IsEmpty => Item == null;
    public Image IconImage => _iconImage;


    public void SetItem(Item item)
    {
        Item = item;
        _iconImage.sprite = item.Icon;
    }

    private void OnValidate()
    {
        if (_validated)
            return;
        
        _text = GetComponentInChildren<TMP_Text>();
    
        int hotkeyNumber = transform.GetSiblingIndex() + 1;
        _text.SetText(hotkeyNumber.ToString());
        gameObject.name = "Slot " + hotkeyNumber.ToString();

        _validated = true;
    }
}