﻿using System;

public interface IPlayerInput
{
    event Action<int> HotkeyPressed;
    event Action MoveModeTogglePressed;
    
    float Vertical { get; }
    float Horitontal { get; }
    float MouseX { get; }
    void Tick();
}