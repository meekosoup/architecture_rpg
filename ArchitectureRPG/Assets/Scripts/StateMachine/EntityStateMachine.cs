﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class EntityStateMachine : MonoBehaviour
{
    private StateMachine _stateMachine = new StateMachine();
    private NavMeshAgent _navMeshAgent;
    private Entity _entity = null;

    public Type CurrentStateType => _stateMachine.CurrentState.GetType();

    private void Awake()
    {
        var player = FindObjectOfType<Player>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _entity = GetComponent<Entity>();

        var idle = new Idle();
        var chasePlayer = new ChasePlayer(_navMeshAgent, player);
        var attack = new Attack();
        var dead = new Dead(_entity);

        if (player != null)
        {
            _stateMachine.AddTransition(
                idle,
                chasePlayer,
                () => DistanceFlat(
                    _navMeshAgent.transform.position,
                    player.transform.position) < 5f);
            
            _stateMachine.AddTransition(
                chasePlayer,
                attack,
                () => DistanceFlat(
                    _navMeshAgent.transform.position,
                    player.transform.position) < 2f);
            
            _stateMachine.AddAnyTransition(
                dead,
                () => IsDead(_entity)
                    );
        }

        _stateMachine.SetState(idle);
    }

    private bool IsDead(Entity entity)
    {
        if (entity != null)
            return entity.Health <= 0;

        return false;
    }
    
    private float DistanceFlat(Vector3 source, Vector3 destination)
    {
        return Vector3.Distance(
            new Vector3(source.x, 0f, source.z), 
            new Vector3(destination.x, 0f, destination.z));
    }
    
    private void Update()
    {
        _stateMachine.Tick();
    }
}