﻿using UnityEngine;

public class ItemInvalidator : ItemComponent
{
    [SerializeField] private string overrideMessage = " was used!";
    public override void Use()
    {
        Debug.Log($"{gameObject.name}{overrideMessage}");
    }
}