﻿using System;
using UnityEngine;

public class PlayerInput : IPlayerInput
{
    public const int HOTBAR_BUTTONS = 9;

    public event Action MoveModeTogglePressed;
    public float Vertical => Input.GetAxis("Vertical");
    public float Horitontal => Input.GetAxis("Horizontal");
    public float MouseX => Input.GetAxis("Mouse X");

    public event Action<int> HotkeyPressed;

    public void Tick()
    {
        for (int i = 0; i < HOTBAR_BUTTONS; i++)
        {
            if (MoveModeTogglePressed != null && Input.GetKeyDown(KeyCode.Minus))
                MoveModeTogglePressed();
            
            if (HotkeyPressed == null)
                return;
            
            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
                HotkeyPressed(i);
        }
    }
}